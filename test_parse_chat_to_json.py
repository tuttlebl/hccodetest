from unittest import TestCase
import json

from chat import parse


def compare_json(a, b):
    try:
        a = json.loads(a)
        b = json.loads(b)
    except ValueError:
        assert False, "json is not valid: {} \n {}".format(a, b)

    keys = ['emoticons', 'mentions']
    for key in keys:
        if key in a and key in b:
            a[key].sort()
            b[key].sort()
            if a[key] != b[key]:
                assert False, "{} lists do not match: {} \n {}".format(key, a, b)
        elif not (key not in a and key not in b):
            assert False, "{} key in only one json object: {} \n {}".format(key, a, b)

    # can't sort the dicts in the list under the links key
    key = 'links'
    if key in a and key in b:
        for item in a[key]:
            if item not in b[key]:
                assert False, "links list don't have the same elements: {} \n {}".format(a, b)
            b[key].remove(item)
        if len(b[key]) > 0:
            assert False, "links list don't have the same elements: {} \n {}".format(a, b)
    elif not (key not in a and key not in b):
        assert False, "Link key in only one json object: {} \n {}".format(a, b)

    return True


class Testjson_compare(TestCase):
    def test_json_full(self):
        a = '''
        {
          "mentions": [
            "bob",
            "john",
            "tim"
          ],
          "emoticons": [
            "success",
            "failure"
          ],
          "links": [
            {
              "url": "https://twitter.com/jdorfman/status/430511497475670016",
              "title": "Twitter / jdorfman: nice @littlebigdetail from ..."
            },
            {
                "a": 1
            }
          ]
        }
        '''
        b = '''
        {
          "mentions": [
            "john",
            "tim",
            "bob"
          ],
          "emoticons": [
            "failure",
            "success"
          ],
          "links": [
            {
                "a": 1
            },
            {
              "url": "https://twitter.com/jdorfman/status/430511497475670016",
              "title": "Twitter / jdorfman: nice @littlebigdetail from ..."
            }
          ]
        }
        '''
        self.assertTrue(compare_json(a, b))

    def test_json_non_match(self):
        a = '{"emoticons": ["hats"]}'
        b = '{"emoticons": ["bats"]}'
        with self.assertRaises(AssertionError):
            compare_json(a, b)


class Testparse_chat_to_json(TestCase):
    def test_emoticon_only(self):
        in_str = "Good morning! (megusta) (coffee)"
        result = '''
        {
          "emoticons": [
            "megusta",
            "coffee"
          ]
        }
        '''
        test_json = parse.parse_chat_to_json(in_str)
        self.assertTrue(compare_json(test_json, result))

    def test_link_only(self):
        in_str = "Olympics are starting soon; http://www.nbcolympics.com"
        result = '''
        {
          "links": [
            {
              "url": "http://www.nbcolympics.com",
              "title": "NBC Olympics | Home of the 2016 Olympic Games in Rio"
            }
          ]
        }
        '''
        test_json = parse.parse_chat_to_json(in_str)
        self.assertTrue(compare_json(test_json, result))

    def test_mention_only(self):
        in_str = "@chris you around?"
        result = '''
        {
          "mentions": [
            "chris"
          ]
        }
        '''
        test_json = parse.parse_chat_to_json(in_str)
        self.assertTrue(compare_json(test_json, result))

    def test_all_tokens(self):
        in_str = "@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016"
        result = '''
        {
          "mentions": [
            "bob",
            "john"
          ],
          "emoticons": [
            "success"
          ],
          "links": [
            {
              "url": "https://twitter.com/jdorfman/status/430511497475670016",
              "title": "Justin Dorfman on Twitter: &quot;nice @littlebigdetail from @HipChat (shows hex colors when pasted in chat). http://t.co/7cI6Gjy5pq&quot;"
            }
          ]
        }
        '''
        test_json = parse.parse_chat_to_json(in_str)
        self.assertTrue(compare_json(test_json, result))

    def test_complex_no_space(self):
        in_str = "(nicehats)@bob maybe you can find more at http://www.amazon.com"
        result = '''
        {
          "mentions": [
            "bob"
          ],
          "emoticons": [
            "nicehats"
          ],
          "links": [
            {
              "url": "http://www.amazon.com",
              "title": "Amazon.com: Online Shopping for Electronics, Apparel, Computers, Books, DVDs &amp; more"
            }
          ]
        }
        '''
        test_json = parse.parse_chat_to_json(in_str)
        self.assertTrue(compare_json(test_json, result))

    def test_emoticon_in_link(self):
        in_str = "https://en.wikipedia.org/wiki/Eppure_sentire_(Un_senso_di_te)"
        result = '''
        {
          "links": [
            {
              "url": "https://en.wikipedia.org/wiki/Eppure_sentire_(Un_senso_di_te)",
              "title": "Eppure sentire (Un senso di te) - Wikipedia, the free encyclopedia"
            }
          ]
        }
        '''
        test_json = parse.parse_chat_to_json(in_str)
        self.assertTrue(compare_json(test_json, result))