# Chat Package

The chat package currently provides a function to tokenize 
chat string markup into JSON. These are Hyperlinks, Mentions, and Emoticons. 
This is via a single function chat.parse.parse_chat_to_json.

##Requirements

Code is written for python 3.4 on Ubuntu 14.04. It has not been tested on other configurations.

## How to use

First import the function

    from chat.parse import parse_chat_to_json

Then simply call against your input chat string

    json = parse_chat_to_json(chat_string)


## Running Tests

To run test

    python3 -m unittest test_parse_chat_to_json.py




