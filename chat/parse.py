"""
Extracts from a chat string the following token patterns.

Link, a URL along with the web pages title.
Emoticon, mark up to indicate an emoticon, basic form is (name).
Mention, mark up which names another chat user, basic form is @user.

The order of application gives precedent for finding a token, for example:
http://www.example.com/not-an-emoticon-(emoticon)
is a a valid url and emoticon.
The assumption is made it should only be one; the order set in _tokenizer_funcs determines which will result.

To Use:
    from chat.parse import parse_chat_to_json
    json = parse_chat_to_json(chat_string)

To Add a New Tokenizer:
    add a new entry to _tokenizer_funcs
    (new key, new tokenizer func)
"""

import json

from chat.tokenizer import Link, Emoticon, Mention


# order indicates precedence
# e.g. a valid name in a url should yield a url and not an emoticon
# [ (key , func to apply), (other key, other func), ... ]
_tokenizer_funcs = [
    ('links', Link()),
    ('emoticons', Emoticon()),
    ('mentions', Mention())
]


def parse_chat_to_json(chat_str):
    """
    Core configuration is the _tokenizer_funcs list structure.
    :param chat_str: String from a chat message
    :return: JSON of tokens for markup
    """
    assert isinstance(chat_str, str), "chat_str must be of type string"

    result_token = {}
    for key, func in _tokenizer_funcs:
        chat_str, token_list = func(chat_str)
        if token_list:
            result_token[key] = token_list
    return json.dumps(result_token)
