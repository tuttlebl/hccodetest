"""
This module defines the Tokenizer class and its children.
Basic usage is to subclass Tokenizer and just define the PATTERN class var.
All distinct matches will be returned in a list.

Class are used so that the regular expression can be compiled at import time of the chat module
as well as for extension of the module's behavior, such as in the Link class.

Entry point should be the __call__ method. The goal is to make the class act like a function.

"""

import re
from urllib.request import urlopen
from urllib.error import URLError, HTTPError


class Tokenizer(object):
    """
    Basic pattern is to simply define PATTERN.
    PATTERN needs one capturing group. That group is what will be returned as the token.
    """
    def __init__(self, *flags):
        """
        *flags for setting regex flags
        """
        self._re_obj = re.compile(self.PATTERN, *flags)

    def __call__(self, in_str):
        """

        :param in_str: chat string to consume
        :returns in_str: chat string with found tokens removed
        :returns token_list: list with each entry as the found token as a string
        """
        token_list = []
        for match in self._re_obj.finditer(in_str):
            token_list.append(match.group(1))
            in_str = '{}{}'.format(in_str[:match.start()], in_str[match.end():])
        return in_str, token_list


class Link(Tokenizer):
    """
    Find URLs in the chat text, extract them, and fetch over the web the page's title.
    
    This will only match fully qualified links with the http(s):// scheme. It would seem plausible to
    investigate parsing out more casual links, but doing so within a larger body of arbitrary text
    will be difficult. 

    Reference material used in my search for a good regex pattern:
    http://blog.codinghorror.com/the-problem-with-urls/
    http://www.regexguru.com/2008/11/detecting-urls-in-a-block-of-text/
    https://mathiasbynens.be/demo/url-regex
    """
    PATTERN = r'\b((?:https?://)[-A-Z0-9+&@#/%=~_|$?!:,.\(]*[A-Z0-9+&@#/%=~_|$\)])'
    SCHEME_PATTERN = r'https?://'
    HEADER_PATTERN = '<title>(.+?)</title>'

    def __init__(self):
        super().__init__(re.IGNORECASE)
        self._header_re_obj = re.compile(self.HEADER_PATTERN, re.IGNORECASE)
        self._scheme_re_obj = re.compile(self.SCHEME_PATTERN)

    def get_url_title(self, url):
        """
        We look for the first <title>Title</title> element.
        If fetching the link title fails just return an empty title.

        :param url: url token string
        :return: string of the page title
        """
        try:
            return self._header_re_obj.search(urlopen(url).read().decode('utf-8')).group(1)
        except (IndexError, URLError, HTTPError, ValueError):
            # errors related to getting the title will yield a empty title
            return ''

    def __call__(self, in_str):
        """

        :param in_str: chat string to consume
        :returns out_str: chat string with found tokens removed
        :returns token_list: a list of dictionaries one entry per token, a key of url and a key of title per token
        """
        out_str, token_list = super().__call__(in_str)
        for i in range(len(token_list)):
            page_title = self.get_url_title(token_list[i])
            token_list[i] = {'url': token_list[i], 'title': page_title}
        return out_str, token_list


class Emoticon(Tokenizer):
    """
    Emoticons - For this exercise, you only need to consider 'custom' emoticons which are ASCII strings, no longer than
    15 characters, contained in parenthesis. You can assume that anything matching this format is an emoticon.
    (http://hipchat-emoticons.nyh.name)
     
    While I can't find anything suggesting emoticons can't have new lines or white space, that seems odd.
    For now, the function only accepts word characters.
    """
    PATTERN = r'\(([\w]{1,15}?)\)'
    def __init__(self):
        super().__init__(re.ASCII)


class Mention(Tokenizer):
    """
    @mentions - A way to mention a user. Always starts with an '@' and ends when hitting a non-word character.
    (http://help.hipchat.com/knowledgebase/articles/64429-how-do-mentions-work-)
    """
    PATTERN = r'@(\w+)\b'
